-- MySQL dump 10.13  Distrib 8.0.26, for Win64 (x86_64)
--
-- Host: localhost    Database: rapid_pay
-- ------------------------------------------------------
-- Server version	5.7.35-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `card`
--

DROP TABLE IF EXISTS `card`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `card` (
  `id_card` char(32) NOT NULL,
  `card_number` char(15) NOT NULL,
  `created_on_utc` datetime NOT NULL,
  `owner_name` varchar(20) NOT NULL,
  `owner_last_name` varchar(20) NOT NULL,
  `credit_limit` decimal(10,2) NOT NULL,
  PRIMARY KEY (`id_card`),
  UNIQUE KEY `card_number_UNIQUE` (`card_number`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `card`
--

LOCK TABLES `card` WRITE;
/*!40000 ALTER TABLE `card` DISABLE KEYS */;
INSERT INTO `card` VALUES ('06044fe7c7dd49799cd26ede3fffcd1c','1234','0001-01-01 00:00:00','Tomas','Aguirre',100.00),('0b9afde7893c49f7b7c358aac26ed866','000000000000006','2022-01-17 19:34:18','Tomas','Aguirre',100.00),('33329900399e4939a5cccc8bdb56bd8c','000000000000009','2022-01-17 21:13:57','Tomaso','Aguirres',25.00),('4ebf35bd4f1e4431ab48cde3519f8ee4','000000000000002','0001-01-01 00:00:00','Tomas','Aguirre',100.00),('61a90ce1f9cd4c65b6c44431db787557','000000000000008','2022-01-17 20:55:27','Tomaso','Aguirres',25.00),('95d1fa1bd12247eca3c7ee4927565652','1235','0001-01-01 00:00:00','Tomas','Aguirre',100.00),('9c4b7c009f2d4f6693d686a1366e16e0','000000000000010','2022-01-17 21:46:45','Tomaso','Aguirres',25.00),('b046b3cc40a24c0ea8dac524c08d01a4','000000000000007','2022-01-17 20:42:03','Tomaso','Aguirres',25.00),('b04e9edfd69e46a8a309031bd51c753f','000000000000005','2022-01-15 01:18:46','Tomas','Aguirre',100.00),('ca6acbb91e8f48e189e255afa9ef2ffd','000000000000001','0001-01-01 00:00:00','Tomas','Aguirre',100.00),('d2c768bdda884f9ead02c8e30e4001fa','1236','0001-01-01 00:00:00','Tomas','Aguirre',100.00),('e9a9c7e33e7f47b3951a0ec51162ab90','000000000000003','0001-01-01 00:00:00','Tomas','Aguirre',100.00);
/*!40000 ALTER TABLE `card` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `payment`
--

DROP TABLE IF EXISTS `payment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `payment` (
  `id_payment` char(32) NOT NULL,
  `id_card` char(32) NOT NULL,
  `created_on_utc` datetime NOT NULL,
  `amount` decimal(10,2) NOT NULL,
  `fee` decimal(10,2) NOT NULL,
  `total` decimal(10,2) NOT NULL,
  PRIMARY KEY (`id_payment`),
  KEY `fk_payment_card_id_card_idx` (`id_card`),
  CONSTRAINT `fk_payment_card_id_card` FOREIGN KEY (`id_card`) REFERENCES `card` (`id_card`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `payment`
--

LOCK TABLES `payment` WRITE;
/*!40000 ALTER TABLE `payment` DISABLE KEYS */;
INSERT INTO `payment` VALUES ('0bb95eab50ec4ee4a9cf2bde38cbc318','61a90ce1f9cd4c65b6c44431db787557','2022-01-17 20:59:37',11.00,4.10,15.10),('26d9c2d3b2814c08b5b967a193c34c94','61a90ce1f9cd4c65b6c44431db787557','2022-01-17 20:55:45',2.00,4.10,6.10),('349eba87506241bc8f84334edbf5e699','b04e9edfd69e46a8a309031bd51c753f','2022-01-17 20:14:57',1.00,4.10,5.10),('35eb4839d8d14473bf871d0ed0db2ff3','b04e9edfd69e46a8a309031bd51c753f','2022-01-15 07:40:48',20.00,2.10,22.10),('465873e0471e47ff944092d3cdf6b945','9c4b7c009f2d4f6693d686a1366e16e0','2022-01-17 21:47:13',11.00,7.50,18.50),('8db0400a249f463aac4f5def9a33bc62','b046b3cc40a24c0ea8dac524c08d01a4','2022-01-17 20:49:04',2.00,4.10,6.10),('90114e94bf2641358583352ba6af6460','b04e9edfd69e46a8a309031bd51c753f','2022-01-15 07:37:01',20.00,2.10,22.10),('a923866d1bc442c5907b9413cc6ea2e5','b04e9edfd69e46a8a309031bd51c753f','2022-01-15 05:54:08',10.00,0.00,10.00),('b7f10e3346ef433bbc184f650f748c7a','b046b3cc40a24c0ea8dac524c08d01a4','2022-01-17 20:44:52',2.00,4.10,6.10),('c5c6c31ee3334f18824da04d521316d9','b04e9edfd69e46a8a309031bd51c753f','2022-01-15 07:38:59',20.00,2.10,22.10),('f247650ae82e46a8bae2a92382510dec','b046b3cc40a24c0ea8dac524c08d01a4','2022-01-17 20:44:38',2.00,4.10,6.10);
/*!40000 ALTER TABLE `payment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `system`
--

DROP TABLE IF EXISTS `system`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `system` (
  `current_card` int(11) NOT NULL,
  `current_fee` decimal(2,1) NOT NULL,
  `fee_last_update_utc` datetime NOT NULL,
  `last_fee` decimal(2,1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `system`
--

LOCK TABLES `system` WRITE;
/*!40000 ALTER TABLE `system` DISABLE KEYS */;
INSERT INTO `system` VALUES (10,0.0,'2022-01-17 22:09:13',0.0);
/*!40000 ALTER TABLE `system` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'rapid_pay'
--

--
-- Dumping routines for database 'rapid_pay'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-01-17 17:29:27
