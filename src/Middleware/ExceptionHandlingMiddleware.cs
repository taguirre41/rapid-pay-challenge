﻿using rapid_pay.Models;
using System.Net;
using System.Text.Json;

namespace rapid_pay.Middleware
{
    public class ExceptionHandlingMiddleware
    {
        private readonly RequestDelegate _next;

        public ExceptionHandlingMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext context)
        {
            try
            {
                await _next(context).ConfigureAwait(false);
            }
            catch (Exception ex)
            {
                context.Response.ContentType = "application/json";
                context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;

                var response = new ResponseBase()
                {
                    ErrorMessage = ex.Message
                };

                await context.Response.WriteAsync(JsonSerializer.Serialize(response)).ConfigureAwait(false);
            }
        }
    }
}
