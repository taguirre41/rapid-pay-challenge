﻿using Microsoft.AspNetCore.Mvc;
using rapid_pay.Domain;
using rapid_pay.Models.Commands;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace rapid_pay.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PaymentController : ControllerBase
    {
        private readonly PaymentFee _paymentFee;
        public PaymentController(PaymentFee paymentFee)
        {
            _paymentFee = paymentFee;
        }

        [HttpPost]
        [Route("IdCard")]
        public async Task<ActionResult> CreatePaymentByIdCardAsync(CreatePaymentByIdCardCommand command)
        {
            command.Validate();
            var paymentData = await _paymentFee.CreatePaymentByIdCardAsync(command).ConfigureAwait(false);

            return Ok(new CreatePaymentResponse { Id = paymentData.Id });
        }



        [HttpPost]
        [Route("CardNumber")]
        public async Task<ActionResult> CreatePaymentByCardNumberAsync(CreatePaymentByCardNumberCommand command)
        {
            command.Validate();
            var paymentData = await _paymentFee.CreatePaymentByCardNumberAsync(command).ConfigureAwait(false);

            return Ok(new CreatePaymentResponse { Id = paymentData.Id });
        }
    }
}
