﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using rapid_pay.Domain;
using rapid_pay.Models.Commands;
using rapid_pay.Models.Queries;

namespace rapid_pay.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CardController : ControllerBase
    {
        private readonly CardManagement _cardManagement;
        public CardController(CardManagement cardManagement)
        {
            _cardManagement = cardManagement;
        }

        [HttpPost]
        [Route("")]
        public async Task<ActionResult> CreateCardAsync(CreateCardCommand command)
        {
            command.Validate();
            var cardData = await _cardManagement.CreateCardAsync(command).ConfigureAwait(false);

            return Ok(new CreateCardResponse
            {
                CardNumber = cardData.Number,
                Id = cardData.Id,
            });
        }

        [HttpGet]
        [Route("balance/idcard/{idCard}")]
        [Authorize]
        public async Task<ActionResult> GetCardBalanceByIdCardAsync(string idCard)
        {
            var query = new GetCardBalanceByIdCardQuery { IdCard = idCard };
            query.Validate();

            var cardBalance = await _cardManagement.GetCardBalanceByIdCardAsync(query).ConfigureAwait(false);

            return Ok(new GetCardBalanceQueryResponse
            {
                CardBalance = cardBalance
            });
        }


        [HttpGet]
        [Route("balance/cardnumber/{cardNumber}")]
        [Authorize]
        public async Task<ActionResult> GetCardBalanceByCardNumberAsync(string cardNumber)
        {
            var query = new GetCardBalanceByCardNumberQuery { CardNumber = cardNumber };
            query.Validate();

            var cardBalance = await _cardManagement.GetCardBalanceByCardNumberAsync(query).ConfigureAwait(false);

            return Ok(new GetCardBalanceQueryResponse
            {
                CardBalance = cardBalance
            });
        }
    }
}
