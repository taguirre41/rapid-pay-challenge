﻿using rapid_pay.Domain.ValueObjects;
using rapid_pay.Infrastructure.Database;
using System.Data;

namespace rapid_pay.Infrastructure
{
    public class UniversalFeesExchangeDataAdapter
    {
        private readonly IDbAdapter _dataAdapter;

        public UniversalFeesExchangeDataAdapter(IDbAdapter dataAdapter)
        {
            _dataAdapter = dataAdapter;
        }

        async internal Task<FeeData> GetCurrentFeeAsync()
        {
            var sql = @"SELECT current_fee, fee_last_update_utc, last_fee
                        FROM system
                        LIMIT 1";

            Func<IDataReader, FeeData> mapToFeeData = (IDataReader reader) =>
            {
                return new FeeData
                {
                    CurrentFee = decimal.Parse(reader["current_fee"].ToString()),
                    LastFee = decimal.Parse(reader["last_fee"].ToString()),
                    LastUpdateUtc = (DateTime)reader["fee_last_update_utc"]
                };
            };

            return await _dataAdapter.ExecuteSingleAsync<FeeData>(sql, mapToFeeData).ConfigureAwait(false);
        }

        async internal Task UpdateCurrentFeeAsync(FeeData feeData)
        {
            var sql = @"UPDATE system 
                        SET current_fee = @current_fee, 
                            last_fee = @last_fee, 
                            fee_last_update_utc = @fee_last_update_utc";

            var parameters = new List<KeyValuePair<string, object>> {
                new KeyValuePair<string, object>("@current_fee", feeData.CurrentFee),
                new KeyValuePair<string, object>("@last_fee", feeData.LastFee),
                new KeyValuePair<string, object>("@fee_last_update_utc", feeData.LastUpdateUtc.ToString("yyyy-MM-dd HH:mm:ss"))
                };

            await _dataAdapter.ExecuteCommandAsync(sql, parameters).ConfigureAwait(false);
        }
    }
}
