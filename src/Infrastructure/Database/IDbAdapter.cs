﻿using System.Data;

namespace rapid_pay.Infrastructure.Database
{
    public interface IDbAdapter
    {
        Task ExecuteCommandAsync(string sqlCommand, IEnumerable<KeyValuePair<string, object>>? parameters = null);
        Task<IEnumerable<T>> ExecuteQueryAsync<T>(string sqlCommand, Func<IDataReader, T> mapToObject, IEnumerable<KeyValuePair<string, object>>? parameters = null);
        Task<object?> ExecuteScalarAsync(string sqlCommand, IEnumerable<KeyValuePair<string, object>>? parameters = null);
        Task<T?> ExecuteSingleAsync<T>(string sqlCommand, Func<IDataReader, T> mapToObject, IEnumerable<KeyValuePair<string, object>>? parameters = null);
    }
}
