﻿using MySql.Data.MySqlClient;
using System.Data;

namespace rapid_pay.Infrastructure.Database
{
    public class MySqlDbAdapter : IDbAdapter, IDisposable
    {
        private readonly MySqlConnection _connection;
        private MySqlTransaction _transaction;
        private bool disposedValue;

        public MySqlDbAdapter(IConfiguration config)
        {
            _connection = new MySqlConnection(config.GetConnectionString("RapidPayConnection"));
            _connection.Open();
            disposedValue = false;
        }

        public void Dispose()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }

        public async Task ExecuteCommandAsync(string sqlCommand, IEnumerable<KeyValuePair<string, object>>? parameters = null)
        {
            var command = BuildCommand(sqlCommand, parameters);
            await command.ExecuteNonQueryAsync().ConfigureAwait(false);
        }

        public async Task<IEnumerable<T>> ExecuteQueryAsync<T>(string sqlCommand, Func<IDataReader, T> mapToObject, IEnumerable<KeyValuePair<string, object>>? parameters = null)
        {
            var command = BuildCommand(sqlCommand, parameters);
            var reader = await command.ExecuteReaderAsync().ConfigureAwait(false);

            var result = new List<T>();

            try
            {
                while (reader.Read())
                {
                    result.Add(mapToObject(reader));
                }
                return result;
            }
            finally
            {
                if (reader != null)
                    reader.Close();
            }
        }

        public async Task<T?> ExecuteSingleAsync<T>(string sqlCommand, Func<IDataReader, T> mapToObject, IEnumerable<KeyValuePair<string, object>>? parameters = null)
        {
            var command = BuildCommand(sqlCommand, parameters);
            var reader = await command.ExecuteReaderAsync().ConfigureAwait(false);

            try
            {
                if (reader.Read())
                    return mapToObject(reader);

                return default;
            }
            finally
            {
                if (reader != null)
                    reader.Close();
            }
        }

        public async Task<object?> ExecuteScalarAsync(string sqlCommand, IEnumerable<KeyValuePair<string, object>>? parameters = null)
        {
            var command = BuildCommand(sqlCommand, parameters);
            var result = await command.ExecuteScalarAsync().ConfigureAwait(false);
            return result is DBNull ? null : result;
        }

        private MySqlCommand BuildCommand(string sqlCommand, IEnumerable<KeyValuePair<string, object>>? parameters)
        {
            var command = new MySqlCommand
            {
                CommandText = sqlCommand,
                CommandType = CommandType.Text,
                Connection = _connection,
            };

            if (parameters != null)
                command.Parameters.AddRange(parameters.Select(p => new MySqlParameter(p.Key, p.Value)).ToArray());

            return command;
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (_transaction != null)
                    _transaction.Dispose();
                _connection.Close();
                _connection.Dispose();
                disposedValue = true;
            }
        }

        ~MySqlDbAdapter()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: false);
        }
    }
}
