﻿using rapid_pay.Domain.ValueObjects;
using rapid_pay.Infrastructure.Database;
using System.Data;

namespace rapid_pay.Infrastructure
{
    public class PaymentDataAdapter
    {
        private readonly IDbAdapter _dataAdapter;

        public PaymentDataAdapter(IDbAdapter dbAdapter)
        {
            _dataAdapter = dbAdapter;
        }

        internal async Task CreatePaymentAsync(PaymentData paymentData)
        {
            var sql = @"INSERT INTO payment (id_payment, id_card, created_on_utc, amount, fee, total)
                        VALUES (@id_payment, @id_card, UTC_TIMESTAMP(), @amount, @fee, @total)";

            var parameters = new List<KeyValuePair<string, object>> {
                new KeyValuePair<string, object>("@id_payment", paymentData.Id),
                new KeyValuePair<string, object>("@id_card", paymentData.IdCard),
                new KeyValuePair<string, object>("@amount", paymentData.Amount),
                new KeyValuePair<string, object>("@fee", paymentData.Fee),
                new KeyValuePair<string, object>("@total", paymentData.Total),
                };

            await _dataAdapter.ExecuteCommandAsync(sql, parameters).ConfigureAwait(false);
        }

        internal async Task<decimal> GetPaymentsSumAsync(string idCard)
        {
            var sql = @"SELECT SUM(total)
                        FROM payment
                        WHERE id_card = @id_card";

            var parameters = new List<KeyValuePair<string, object>> {
                new KeyValuePair<string, object>("@id_card", idCard)
                };

            var result = await _dataAdapter.ExecuteScalarAsync(sql, parameters).ConfigureAwait(false);
            return result != null ? Convert.ToDecimal(result) : 0.00m;
        }

        internal async Task<IEnumerable<PaymentData>> ListPaymentsAsync(string idCard)
        {
            var sql = @"SELECT id_payment, created_on_utc, total, amount
                        FROM payment
                        WHERE id_card = @id_card";

            var parameters = new List<KeyValuePair<string, object>> {
                new KeyValuePair<string, object>("@id_card", idCard)
                };

            Func<IDataReader, PaymentData> mapToPaymentData = (IDataReader reader) =>
            {
                return new PaymentData
                {
                    Id = reader["id_payment"].ToString(),
                    Amount = decimal.Parse(reader["amount"].ToString()),
                    CreatedOnUtc = (DateTime)reader["created_on_utc"],
                    Total = decimal.Parse(reader["total"].ToString())
                };
            };

            return await _dataAdapter.ExecuteQueryAsync<PaymentData>(sql, mapToPaymentData, parameters).ConfigureAwait(false);
        }

    }
}
