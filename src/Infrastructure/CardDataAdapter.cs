﻿using rapid_pay.Domain.ValueObjects;
using rapid_pay.Infrastructure.Database;
using System.Data;

namespace rapid_pay.Infrastructure
{
    public class CardDataAdapter
    {
        private readonly IDbAdapter _dataAdapter;
        private readonly SemaphoreSlim _lock = new(1, 1);

        public CardDataAdapter(IDbAdapter dataAdapter)
        {
            _dataAdapter = dataAdapter;
        }

        internal async Task CreateCardAsync(CardData cardData)
        {
            var sql = @"INSERT INTO card (id_card, card_number, created_on_utc, owner_name, owner_last_name, credit_limit)
                        VALUES (@id_card, @card_number, UTC_TIMESTAMP(), @owner_name, @owner_last_name, @credit_limit)";

            var parameters = new List<KeyValuePair<string, object>> {
                new KeyValuePair<string, object>("@id_card", cardData.Id),
                new KeyValuePair<string, object>("@card_number", cardData.Number),
                new KeyValuePair<string, object>("@owner_name", cardData.OwnerName),
                new KeyValuePair<string, object>("@owner_last_name", cardData.OwnerLastName),
                new KeyValuePair<string, object>("@credit_limit", cardData.CreditLimit),
                };

            await _dataAdapter.ExecuteCommandAsync(sql, parameters).ConfigureAwait(false);
        }

        internal async Task<bool> CardNumberExistsAsync(string cardNumber)
        {
            var sql = @"SELECT EXISTS(SELECT 1 FROM card WHERE card_number = @card_number)";

            var parameters = new List<KeyValuePair<string, object>>
            {
                new KeyValuePair<string, object>("@card_number", cardNumber)
            };

            var result = await _dataAdapter.ExecuteScalarAsync(sql, parameters).ConfigureAwait(false);
            return (result != null) && Convert.ToBoolean(result);
        }

        internal async Task<bool> CardIdExistsAsync(string id)
        {
            var sql = @"SELECT EXISTS(SELECT 1 FROM card WHERE id_card = @id_card)";

            var parameters = new List<KeyValuePair<string, object>>
            {
                new KeyValuePair<string, object>("@id_card", id)
            };

            var result = await _dataAdapter.ExecuteScalarAsync(sql, parameters).ConfigureAwait(false);
            return (result != null) && Convert.ToBoolean(result);
        }

        internal async Task<CardData> GetCardByIdCardAsync(string id)
        {
            var sql = @"SELECT id_card, card_number, created_on_utc, owner_name, owner_last_name, credit_limit
                        FROM card
                        WHERE id_card = @id_card";

            var parameters = new List<KeyValuePair<string, object>>
            {
                new KeyValuePair<string, object>("@id_card", id)
            };

            return await _dataAdapter.ExecuteSingleAsync<CardData>(sql, MapToCardData, parameters).ConfigureAwait(false);
        }

        internal async Task<CardData> GetCardByNumberAsync(string cardNumber)
        {
            var sql = @"SELECT id_card, card_number, created_on_utc, owner_name, owner_last_name, credit_limit
                        FROM card
                        WHERE card_number = @card_number";

            var parameters = new List<KeyValuePair<string, object>>
            {
                new KeyValuePair<string, object>("@card_number", cardNumber)
            };

            return await _dataAdapter.ExecuteSingleAsync<CardData>(sql, MapToCardData, parameters).ConfigureAwait(false);
        }

        internal async Task<int> GetNextCardNumberAsync()
        {
            await _lock.WaitAsync().ConfigureAwait(false);
            try
            {
                var sqlUpdateNextNumber = @"UPDATE system SET current_card = current_card + 1";
                await _dataAdapter.ExecuteCommandAsync(sqlUpdateNextNumber).ConfigureAwait(false);

                var sqlQueryNextNumber = @"SELECT current_card FROM system LIMIT 1";
                var result = await _dataAdapter.ExecuteScalarAsync(sqlQueryNextNumber).ConfigureAwait(false);
                return result == null ? 1 : (int)result;
            }
            finally
            {
                _lock.Release();
            }
        }

        private CardData MapToCardData(IDataReader reader)
        {
            return new CardData
            {
                Id = reader["id_card"].ToString(),
                Number = reader["card_number"].ToString(),
                CreatedOnUtc = (DateTime)reader["created_on_utc"],
                OwnerName = reader["owner_name"].ToString(),
                OwnerLastName = reader["owner_last_name"].ToString(),
                CreditLimit = decimal.Parse(reader["credit_limit"].ToString())
            };
        }
    }
}
