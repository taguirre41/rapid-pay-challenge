﻿namespace rapid_pay.Exceptions
{
    public class CreditCardException : Exception
    {
        public CreditCardException(string message) : base(message)
        {
        }
    }
}
