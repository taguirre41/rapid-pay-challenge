﻿namespace rapid_pay.Exceptions
{
    public class PaymentsException : Exception
    {
        public PaymentsException(string message) : base(message)
        {
        }
    }
}
