﻿using rapid_pay.Infrastructure;

namespace rapid_pay.Domain
{
    public class UniversalFeesExchange
    {
        private const double _minFee = 0.1;
        private const double _maxFee = 2.0;
        private readonly SemaphoreSlim _lock = new(1, 1);
        private readonly UniversalFeesExchangeDataAdapter _dataAdapter;
        public UniversalFeesExchange(UniversalFeesExchangeDataAdapter dataAdapter)
        {
            _dataAdapter = dataAdapter;
        }

        internal async Task<decimal> GetFee()
        {
            await _lock.WaitAsync().ConfigureAwait(false);
            try
            {
                var feeData = await _dataAdapter.GetCurrentFeeAsync().ConfigureAwait(false);
                var currentDateTimeUtc = DateTime.UtcNow;

                var hours = (currentDateTimeUtc - feeData.LastUpdateUtc).TotalHours;

                if (hours >= 1)
                {
                    while (hours >= 1)
                    {
                        var random = new Random();
                        var randomNumber = (decimal)(_minFee + (random.NextDouble() * (_maxFee - _minFee)));
                        var randomDecimal = Math.Round(randomNumber, 1);

                        feeData.LastFee = feeData.CurrentFee;
                        feeData.CurrentFee = randomDecimal * feeData.LastFee;
                        hours--;
                    }
                    feeData.LastUpdateUtc = currentDateTimeUtc;

                    await _dataAdapter.UpdateCurrentFeeAsync(feeData).ConfigureAwait(false);
                }

                return feeData.CurrentFee;
            }
            finally
            {
                _lock.Release();
            }
        }
    }
}
