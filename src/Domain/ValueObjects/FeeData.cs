﻿namespace rapid_pay.Domain.ValueObjects
{
    internal class FeeData
    {
        public decimal CurrentFee { get; set; }
        public decimal LastFee { get; set; }
        public DateTime LastUpdateUtc { get; set; }

    }
}
