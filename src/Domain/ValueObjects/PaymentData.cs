﻿namespace rapid_pay.Domain.ValueObjects
{
    internal class PaymentData
    {
        public string Id { get; set; }
        public string IdCard { get; set; }
        public DateTime CreatedOnUtc { get; set; }
        public decimal Amount { get; set; }
        public decimal Fee { get; set; }
        public decimal Total { get; set; }
    }
}
