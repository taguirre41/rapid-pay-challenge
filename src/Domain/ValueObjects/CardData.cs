﻿namespace rapid_pay.Domain.ValueObjects

{
    internal class CardData
    {
        public string Id { get; set; }
        public string Number { get; set; }
        public string OwnerName { get; set; }
        public string OwnerLastName { get; set; }
        public DateTime CreatedOnUtc { get; set; }
        public decimal CreditLimit { get; set; }
    }
}
