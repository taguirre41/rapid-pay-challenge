﻿using rapid_pay.Domain.ValueObjects;
using rapid_pay.Exceptions;
using rapid_pay.Infrastructure;
using rapid_pay.Models;
using rapid_pay.Models.Commands;
using rapid_pay.Models.Queries;

namespace rapid_pay.Domain
{
    public class CardManagement
    {
        private readonly PaymentDataAdapter _paymentDataAdapter;
        private readonly CardDataAdapter _cardDataAdapter;
        public CardManagement(PaymentDataAdapter paymentDataAdapter, CardDataAdapter cardDataAdapter)
        {
            _paymentDataAdapter = paymentDataAdapter;
            _cardDataAdapter = cardDataAdapter;
        }
        internal async Task<CardData> CreateCardAsync(CreateCardCommand command)
        {
            var nextNumber = await _cardDataAdapter.GetNextCardNumberAsync().ConfigureAwait(false);

            var card = new CardData
            {
                Id = Guid.NewGuid().ToString("N"),
                Number = nextNumber.ToString().PadLeft(15, '0'),
                OwnerLastName = command.OwnerLastName,
                OwnerName = command.OwnerName,
                CreditLimit = command.CreditLimit
            };

            if (await _cardDataAdapter.CardNumberExistsAsync(card.Number).ConfigureAwait(false))
                throw new CreditCardException("Cannot create card. Number is already taken.");

            await _cardDataAdapter.CreateCardAsync(card).ConfigureAwait(false);
            return card;
        }

        internal async Task<CardBalance> GetCardBalanceByIdCardAsync(GetCardBalanceByIdCardQuery query)
        {
            var card = await _cardDataAdapter.GetCardByIdCardAsync(query.IdCard).ConfigureAwait(false);
            return await GetCardBalance(card).ConfigureAwait(false);
        }

        internal async Task<CardBalance> GetCardBalanceByCardNumberAsync(GetCardBalanceByCardNumberQuery query)
        {
            var card = await _cardDataAdapter.GetCardByNumberAsync(query.CardNumber).ConfigureAwait(false);
            return await GetCardBalance(card).ConfigureAwait(false);
        }

        private async Task<CardBalance> GetCardBalance(CardData card)
        {
            var payments = await _paymentDataAdapter.ListPaymentsAsync(card.Id).ConfigureAwait(false);

            var cardBalance = new CardBalance
            {
                CardNumber = card.Number,
                OwnerFullName = $"{card.OwnerName} {card.OwnerLastName}",
                CreditLimit = card.CreditLimit
            };

            if (payments != null)
            {
                cardBalance.Details.AddRange(payments.Select(p =>
                {
                    var detail = new CardBalance.PaymentDetail
                    {
                        Amount = p.Amount,
                        PayedOn = p.CreatedOnUtc.ToLocalTime(),
                        TotalPayed = p.Total
                    };
                    return detail;
                }));

            }
            return cardBalance;
        }
    }
}
