﻿using rapid_pay.Domain.ValueObjects;
using rapid_pay.Exceptions;
using rapid_pay.Infrastructure;
using rapid_pay.Models.Commands;

namespace rapid_pay.Domain
{
    public class PaymentFee
    {
        private readonly PaymentDataAdapter _paymentDataAdapter;
        private readonly CardDataAdapter _cardDataAdapter;
        private readonly UniversalFeesExchange _universalFeesExchange;
        public PaymentFee(PaymentDataAdapter paymentDataAdapter, CardDataAdapter cardDataAdapter, UniversalFeesExchange universalFeesExchange)
        {
            _paymentDataAdapter = paymentDataAdapter;
            _cardDataAdapter = cardDataAdapter;
            _universalFeesExchange = universalFeesExchange;
        }

        internal async Task<PaymentData> CreatePaymentByIdCardAsync(CreatePaymentByIdCardCommand command)
        {
            if (!await _cardDataAdapter.CardIdExistsAsync(command.IdCard).ConfigureAwait(false))
                throw new CreditCardException("Specified Card ID does not exist");

            var card = await _cardDataAdapter.GetCardByIdCardAsync(command.IdCard).ConfigureAwait(false);
            return await CreatePayment(card.Id, card.CreditLimit, command.Amount).ConfigureAwait(false);
        }

        internal async Task<PaymentData> CreatePaymentByCardNumberAsync(CreatePaymentByCardNumberCommand command)
        {
            if (!await _cardDataAdapter.CardNumberExistsAsync(command.CardNumber).ConfigureAwait(false))
                throw new CreditCardException("Specified Card Number does not exist");

            var card = await _cardDataAdapter.GetCardByNumberAsync(command.CardNumber).ConfigureAwait(false);
            return await CreatePayment(card.Id, card.CreditLimit, command.Amount).ConfigureAwait(false);
        }

        private async Task<PaymentData> CreatePayment(string idCard, decimal cardCreditLimit, decimal paymentAmount)
        {
            var payment = new PaymentData
            {
                Id = Guid.NewGuid().ToString("N"),
                IdCard = idCard,
                Amount = paymentAmount
            };
            payment.Fee = await _universalFeesExchange.GetFee().ConfigureAwait(false);
            payment.Total = payment.Amount + payment.Fee;

            var paymentsSum = await _paymentDataAdapter.GetPaymentsSumAsync(idCard).ConfigureAwait(false);
            var remainingBalance = cardCreditLimit - paymentsSum;

            if (remainingBalance < payment.Total)
                throw new PaymentsException("Not enough amount available for this payment.");

            await _paymentDataAdapter.CreatePaymentAsync(payment).ConfigureAwait(false);
            return payment;
        }

    }
}
