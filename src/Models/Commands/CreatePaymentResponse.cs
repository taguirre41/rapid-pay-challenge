﻿namespace rapid_pay.Models.Commands
{
    public class CreatePaymentResponse : ResponseBase
    {
        public string Id { get; set; }
    }
}
