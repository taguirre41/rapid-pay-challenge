﻿namespace rapid_pay.Models.Commands
{
    public class CreateCardResponse : ResponseBase
    {
        public string Id { get; set; }
        public string CardNumber { get; set; }
    }
}
