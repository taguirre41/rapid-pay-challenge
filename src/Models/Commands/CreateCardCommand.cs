﻿using FluentValidation;
using System.ComponentModel.DataAnnotations;

namespace rapid_pay.Models.Commands
{
    public class CreateCardCommand
    {
        public string OwnerName { get; set; }
        public string OwnerLastName { get; set; }

        [Required]
        public decimal CreditLimit { get; set; }

        public void Validate() =>
            new CreateCardCommandValidator().ValidateAndThrow(this);

        internal class CreateCardCommandValidator : AbstractValidator<CreateCardCommand>
        {
            public CreateCardCommandValidator()
            {
                RuleFor(c => c.OwnerName).NotEmpty();
                RuleFor(c => c.OwnerLastName).NotEmpty();
                RuleFor(c => c.CreditLimit).GreaterThan(0);
            }
        }
    }
}
