﻿using FluentValidation;

namespace rapid_pay.Models.Commands
{
    public class CreatePaymentByCardNumberCommand
    {
        public string CardNumber { get; set; }
        public decimal Amount { get; set; }

        public void Validate() =>
            new CreatePaymentByCardNumberCommandValidator().ValidateAndThrow(this);

        internal class CreatePaymentByCardNumberCommandValidator : AbstractValidator<CreatePaymentByCardNumberCommand>
        {
            public CreatePaymentByCardNumberCommandValidator()
            {
                RuleFor(c => c.CardNumber).NotEmpty();
                RuleFor(c => c.Amount).GreaterThan(0);
            }
        }
    }
}
