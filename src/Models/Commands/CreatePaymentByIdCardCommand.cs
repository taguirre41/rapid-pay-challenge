﻿using FluentValidation;

namespace rapid_pay.Models.Commands
{
    public class CreatePaymentByIdCardCommand
    {
        public string IdCard { get; set; }
        public decimal Amount { get; set; }

        public void Validate() =>
            new CreatePaymentByIdCardCommandValidator().ValidateAndThrow(this);

        internal class CreatePaymentByIdCardCommandValidator : AbstractValidator<CreatePaymentByIdCardCommand>
        {
            public CreatePaymentByIdCardCommandValidator()
            {
                RuleFor(c => c.IdCard).NotEmpty();
                RuleFor(c => c.Amount).GreaterThan(0);
            }
        }
    }
}
