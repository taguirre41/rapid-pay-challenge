﻿using FluentValidation;

namespace rapid_pay.Models.Queries
{
    public class GetCardBalanceByIdCardQuery
    {
        public string IdCard { get; set; }

        public void Validate() =>
            new GetCardBalanceByIdCardQueryValidator().ValidateAndThrow(this);

        internal class GetCardBalanceByIdCardQueryValidator : AbstractValidator<GetCardBalanceByIdCardQuery>
        {
            public GetCardBalanceByIdCardQueryValidator()
            {
                RuleFor(c => c.IdCard).NotEmpty();
            }
        }
    }
}
