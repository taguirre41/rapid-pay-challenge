﻿using FluentValidation;

namespace rapid_pay.Models.Queries
{
    public class GetCardBalanceByCardNumberQuery
    {
        public string CardNumber { get; set; }

        public void Validate() =>
            new GetCardBalanceByCardNumberQueryValidator().ValidateAndThrow(this);

        internal class GetCardBalanceByCardNumberQueryValidator : AbstractValidator<GetCardBalanceByCardNumberQuery>
        {
            public GetCardBalanceByCardNumberQueryValidator()
            {
                RuleFor(c => c.CardNumber).NotEmpty();
            }
        }
    }
}
