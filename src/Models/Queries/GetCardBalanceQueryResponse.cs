﻿namespace rapid_pay.Models.Queries
{
    public class GetCardBalanceQueryResponse : ResponseBase
    {
        public CardBalance CardBalance { get; set; }
    }
}
