﻿namespace rapid_pay.Models
{
    public class CardBalance
    {
        public string CardNumber { get; set; }
        public string OwnerFullName { get; set; }
        public decimal CreditLimit { get; set; }
        public decimal TotalPayed
        {
            get
            {
                return Details.Select(d => d.TotalPayed).Sum();
            }
        }
        public decimal RemainingBalance
        {
            get
            {
                return CreditLimit - TotalPayed;
            }
        }
        public List<PaymentDetail> Details { get; }

        public CardBalance()
        {
            Details = new List<PaymentDetail>();
        }
        public class PaymentDetail
        {
            public decimal Amount { get; set; }
            public DateTime PayedOn { get; set; }
            public decimal TotalPayed { get; set; }

        }
    }
}
